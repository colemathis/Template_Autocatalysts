# Template Autocatalysts

Exploring the role of templating to understand inorganic autocatalysts

So far everything has been done using jupyter notebooks and Julia 

To use this you'll need:
- Jupyter https://jupyter.org/ (can be installed with either conda or pip)
- Julia https://julialang.org/ (don't forget to add it to your path!)

Once Julia is installed you'll need the following packages:
* IJulia https://github.com/JuliaLang/IJulia.jl
* DifferentialEquations.jl (https://diffeq.sciml.ai/stable/)
* DiffEqBiological.jl (https://diffeq.sciml.ai/v6.8/apis/diffeqbio/)
* Plots.jl (https://diffeq.sciml.ai/v6.8/apis/diffeqbio/)
     
